// Base directory to make require absolutly
const root = __dirname + '/';

// Base directory to make require of webpack configuration files
const rootWebpackConfig = root;

const gulp = require('gulp');

// Gulp Task
const gulpTasks = require('./src/js/gulp-tasks/');

// ************************************* LESS *********************************

const lessOptions = { globs: [root + 'src/less/main.less'], dest: root + 'dist/css/' };

gulp.task('less:dev', () => {
	gulpTasks.lessDev(lessOptions);
});

gulp.task('less', () => {
	gulpTasks.lessProd(lessOptions);
});

const watchList = [
	{
		globs: [root + 'src/less/**/*.less'],
		task: 'less:dev',
	},
];

gulp.task('watch', ()=> {
	gulpTasks.watch(watchList);
});

// ************************************* WEBPACK *********************************

gulp.task('webpack-dev-server', () => {
	gulpTasks.webpackDevServer(rootWebpackConfig);
});

gulp.task('webpack', callback => {
	gulpTasks.webpack(callback, {globDelete: [root + 'dist/**'], rootWebpackConfig: rootWebpackConfig});
});

// ************************************* MAIN ENTRIES *********************************

gulp.task('default', ['less:dev', 'watch', 'webpack-dev-server']);

gulp.task('build', ['less', 'webpack']);
