const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const less = require('gulp-less');

function lessDev(options) {
	return gulp.src(options.globs)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(less({
			paths: ['node_modules'],
		}))
		.pipe(sourcemaps.write(options.dest, {
			sourceMappingURL: function(file) {
				return file.relative + '.map';
			}
		})) // write on disk sourcemaps files
		.pipe(gulp.dest(options.dest));
}

module.exports = lessDev;
