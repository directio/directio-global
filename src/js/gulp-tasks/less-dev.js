const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const less = require('gulp-less');
const notifier = require('node-notifier');

const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');

function lessDev(options) {
	const postcssPlugins = [
		autoprefixer({ browsers: ['>0%'] }),
	];

	return gulp.src(options.globs)
		.pipe(plumber(function(err) {
			notifier.notify({
				title: 'Gulp error!',
				message: err.message
			})
			console.log(err.toString());
			this.emit('end');
		}))
		.pipe(sourcemaps.init())
		.pipe(less({
			paths: ['node_modules'],
		}))
		.pipe(postcss(postcssPlugins))
		.pipe(sourcemaps.write(options.dest, {
			sourceMappingURL: function(file) {
				return file.relative + '.map';
			}
		})) // write on disk sourcemaps files
		.pipe(gulp.dest(options.dest));
}

module.exports = lessDev;
