const path = require('path');
const webpack = require('webpack');
const gutil = require('gulp-util');
const del = require('del');

function webpackFunc(callback, optionsWebpack) {
	process.env.NODE_ENV = 'production';
	const webpackProdConfig = require(path.join(optionsWebpack.rootWebpackConfig, './webpack.build.config.js'));
	webpackProdConfig.output.publicPath = '/bundles/';
	const myConfig = Object.assign({}, webpackProdConfig);
	// clean directory
	del.sync(optionsWebpack.globDelete);

	webpack(myConfig, (err, stats)=> { // https://github.com/webpack/docs/wiki/node.js-api
		if (err) throw new gutil.PluginError('webpack', err);
		gutil.log('[webpack]', stats.toString({
			source: false,
			reasons: false,
			chunks: false,
		}));
		callback();
	});
}

module.exports = webpackFunc;
