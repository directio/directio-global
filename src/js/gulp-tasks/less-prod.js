const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const less = require('gulp-less');
const rename = require('gulp-rename');
const cssnano = require('gulp-cssnano');
const clone = require('gulp-clone');
const merge = require('merge-stream');

function lessProd(options) {
	const filesStream = gulp.src(options.globs);

	const lessNormal = filesStream
		.pipe(clone())
		.pipe(sourcemaps.init())
		.pipe(less({
			paths: ['node_modules'],
		}))
		.pipe(cssnano())
		.pipe(rename(function (path) {
			path.basename += ".min";
		}))
		.pipe(sourcemaps.write(options.dest, {
			sourceMappingURL: function(file) {
				return file.relative + '.map';
			}
		}))
		.pipe(gulp.dest(options.dest));

	const lessMinify = filesStream
		.pipe(sourcemaps.init())
		.pipe(less({
			paths: ['node_modules'],
		}))
		.pipe(sourcemaps.write(options.dest, {
			sourceMappingURL: function(file) {
				return file.relative + '.map';
			}
		}))
		.pipe(gulp.dest(options.dest));

	return merge(lessNormal, lessMinify);
}

module.exports = lessProd;
