const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const less = require('gulp-less');
const rename = require('gulp-rename');
const clone = require('gulp-clone');
const merge = require('merge-stream');
const notifier = require('node-notifier');

const cleanCSS = require('gulp-clean-css');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

function lessProd(options) {
	const postcssPluginNormal = [
		autoprefixer(),
	];

	const postcssPluginMinify = postcssPluginNormal.slice();
	postcssPluginMinify.push(cssnano());

	const filesStream = gulp.src(options.globs);

	const lessNormal = filesStream
	.pipe(sourcemaps.init())
	.pipe(less({
		paths: ['node_modules'],
	}))
	.pipe(postcss(postcssPluginNormal))
	.pipe(sourcemaps.write(options.dest, {
		sourceMappingURL: function(file) {
			return file.relative + '.map';
		}
	}))
	.pipe(gulp.dest(options.dest));

	const lessMinify = filesStream
		.pipe(clone())
		.pipe(sourcemaps.init())
		.pipe(less({
			paths: ['node_modules'],
		}))
		.pipe(postcss(postcssPluginMinify))
		.pipe(rename(function (path) {
			path.basename += ".min";
		}))
		.pipe(sourcemaps.write(options.dest, {
			sourceMappingURL: function(file) {
				return file.relative + '.map';
			}
		}))
		.pipe(gulp.dest(options.dest));

	return merge(lessNormal, lessMinify);
}

module.exports = lessProd;
