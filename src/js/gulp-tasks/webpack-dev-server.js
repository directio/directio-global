const path = require('path');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const gutil = require('gulp-util');

function webpackDevServer(rootWebpackConfig) {
	// first set env variables
	process.env.NODE_ENV = 'development';
	const webpackDevConfig = require(path.join(rootWebpackConfig, 'webpack.dev.config'));

	// Start a webpack-dev-server
	const myConfig = Object.create(webpackDevConfig);
	myConfig.debug = true;

	// Start a webpack-dev-server
	new WebpackDevServer(webpack(myConfig), {
		contentBase: '.', // Directory of index.html
		progress: true,
		// webpack-dev-middleware options
		stats: { colors: true},
		quiet: false,
		colors: true,
		noColors: false,
		noInfo: false,
	}).listen(8080, 'localhost', (err) => {
		if (err) throw new gutil.PluginError('webpack-dev-server', err);
		gutil.log('[GULP][webpack-dev-server]', myConfig.output.publicPath);
	});
}

module.exports = webpackDevServer;
