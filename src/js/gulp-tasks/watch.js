const gulp = require('gulp');
const gutil = require('gulp-util');

function watch(files) {
	const filesLen = files.length;
	for (var i = filesLen - 1; i >= 0; i--) {
		return function(file) {
			gulp.watch(file.globs, event => {
				gutil.log('E\' stato cambiato il file: ' + event.path);
				gulp.start(file.task);
			});
		}(files[i])
	}
}

module.exports = watch;
