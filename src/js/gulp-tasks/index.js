const lessDev = require('./less-dev');
const lessProd = require('./less-prod');
const watch = require('./watch');
const webpackDevServer = require('./webpack-dev-server');
const webpack = require('./webpack');

module.exports = {
	lessDev,
	lessProd,
	watch,
	webpackDevServer,
	webpack,
}
