const webpackConfig = require('./webpack-config');

module.exports = webpackConfig({
	hot: false,
	hash: false,
	debug: false,
	optimize: true, // enable UglifyJsPlugin
	progress: true,
	profile: true,
	bail: true,
	saveStats: false,
	failOnError: true,
	devTool: 'source-map',
	banner: true,
});
