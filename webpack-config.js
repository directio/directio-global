const merge = require('lodash.merge');
const webpack = require('webpack');
const path = require('path');
const pkg = require('./package.json');

module.exports = function exportsOptions(options) {
	const defaultOptions = {
		hot: false,
		hash: false,
		debug: false,
		progress: false,
		profile: false,
		bail: false,
		optimize: false,
		saveStats: false,
		failOnError: false,
		banner: false,
	};

	/* eslint-disable no-param-reassign*/
	options = merge(defaultOptions, options || {});

	const loaders = [
		// transpile from ES6 to ES5
		{ test: /\.js?$/, exclude: /node_modules/, loader: 'babel-loader',
			query: {
				cacheDirectory: true,
				// plugins: ['transform-runtime'],
				plugins: ['transform-flow-strip-types'],
				presets: ['es2015', 'stage-0'],
			},
		},
		{ test: /\.dust$/, loader: 'dust-loader-complete', exclude: /node_modules/, query: { verbose: true } },
		// { test: /\.less$/, loader: 'css!less' },
	];

	const preLoaders = [
		// lint es6 files
		// { test: /\.(js|jsx)$/, loader: 'eslint-loader', exclude: /(node_modules|vendor)/ },
	];

	const plugins = [
		new webpack.DefinePlugin({
			'process.env': JSON.stringify({ NODE_ENV: process.env.NODE_ENV}),
			VERSION: pkg.version,
			__DEV__: JSON.parse(process.env.NODE_ENV === 'development' || 'false'),
			__PRERELEASE__: JSON.parse(process.env.BUILD_PRERELEASE || 'false'),
		}),
	];

	if (options.hot) {
		plugins.push(new webpack.HotModuleReplacementPlugin());
	}

	if (options.optimize) {
		// Minimize all JavaScript output of chunks
		plugins.push(new webpack.optimize.UglifyJsPlugin({
			compressor: {
				warnings: false,
			},
			output: {
				comments: false,
			},
		}));
		// plugins.push(new webpack.optimize.DedupePlugin());
		// plugins.push(new webpack.NoErrorsPlugin());
	}

	//	Adds a banner to the top of each generated chunk
	if (options.banner) {
		const banner = 'Name: ' + pkg.name + '\n' + 'Version: ' + pkg.version + '\n' + 'Author: ' + pkg.author;
		plugins.push(new webpack.BannerPlugin(banner));
	}
	// genera in automatico questo file

	const config = {
		// ENTRY
		entry: {
			'dist/js/main': './src/js/index',
		},
		resolve: {
			extensions: ['', '.js'],
			modulesDirectories: ['./node_modules/'],
		},
		output: {
			path: path.join(__dirname, '.'), // percorso per la pubblicazione
			filename: '[name].js', // The filename of the entry chunk (see entry)
			chunkFilename: options.hash ? '[chunkhash].js' : '[name].chunk.js',// The filename of non-entry chunks as relative path inside the output.path directory [id]-[hash]-
			publicPath: 'http://localhost:8080/', // percorso dev altrimenti non funzionano i chunk con VS
			sourceMapFilename: '[name].js.map',
			pathinfo: false,
		},
		externals: {
			// you can use require but the script can be loaded from CDN
			// Loaders are not applied to externals You can (need to) externalize a request with loader: require("bundle!jquery")
			jquery: 'jQuery',
			ga: 'ga',
			Raven: 'Raven',
		},
		module: {
			// PRELOADERS
			preLoaders: preLoaders,
			// LOADERS
			loaders: loaders,
		},
		// PLUGINS
		plugins: plugins,
		debug: options.debug,
		progress: options.progress,
		profile: options.profile,
		bail: options.bail,
		devtool: options.devTool,
	};

	return config;
};
